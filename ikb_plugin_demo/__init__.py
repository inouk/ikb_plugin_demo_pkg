""" Buildit Tasks Demo for plugin as a package in its own repo. """
import os
import pathlib
import re
import json
import configparser
from string import Template
from invoke import task, Collection

from inouk.buildit.tasks import custom_task_init



def init(c, part):
    print("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy ikb_plugin_demo init")
    pass


@task
def custom_task(c):
    """Example of custom task"""
    parts = custom_task_init(c, 'ikb_plugin_demo')
    print("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy ikb_plugin_demo custom_task parts=%s" % parts)    
    pass


def install(c, part):
    print("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy ikb_plugin_demo install")
    pass
