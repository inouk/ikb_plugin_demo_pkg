Install with one of these after '--src' param value adjustment:

    pip install -e git+ssh://git@gitlab.com/inouk/ikb_plugin_demo_pkg.git#egg=ikb_plugin_demo  --src=../egg_sources
    pip install -e git+https://gitlab.com/inouk/ikb_plugin_demo_pkg.git#egg=ikb_plugin_demo --src=../egg_sources
