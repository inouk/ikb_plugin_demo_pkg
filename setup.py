import setuptools

setuptools.setup(
    name="ikb_plugin_demo",
    version="0.0.1",
    packages=setuptools.find_packages(),
    python_requires='>=3.6',
)